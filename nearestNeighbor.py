import numpy as np
import random
from sklearn import neighbors


"""
For each point to predict, we find n nearest point from training set and identify the values for those n nearest point.
The value for new point is the maximum value among the n nearest points. Then add a random number between 0 to standard
deviation of all values in training data to optimize the solution.

"""

## Load training data
train_X = np.genfromtxt('train_X_final.csv', delimiter=',')
train_Y = np.genfromtxt('train_Y_final.csv', delimiter=',')

## Load data
X = np.genfromtxt('val_X_final.csv', delimiter=',')
Y = np.ones(len(X))

## Calculate mean and standard deviation of all values in training data
sd = round(np.std(train_Y))
m = round(np.mean(train_Y))

## define number of neighbors
nn = 5


## model the nearest neighbor
regr = neighbors.KNeighborsRegressor(n_neighbors=nn, weights='uniform')
regr.fit(train_X, train_Y)


## find the nearest neighbor for all new points
result = open('val_Y_final.csv', 'w')
for i in range(len(Y)):
	raw_nn_neighbors = regr.kneighbors(X[i])
	nn_neighbors = raw_nn_neighbors[1][0]
	neighbor_values = np.ones(nn)*-1

	## Identify maximum of values among nearest neighbors
	for j in range(nn):
		neighbor_values[j] = train_Y[nn_neighbors[j]]
	Y[i] = round(max(neighbor_values)) + random.randint(0, sd);

	if Y[i] > 600:
		Y[i] = 600

        ## print the prediction to 'val_Y_final.csv'
	result.write(str(Y[i]) + "\n")

result.close()






